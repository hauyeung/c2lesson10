﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace csharp2_lesson10
{
    public class Shoe
    {
        double size { get; set; }
        string color { get; set; }
        string manufacturer { get; set; }
        string style { get; set; }
        string date { get; set; }
    }
}
